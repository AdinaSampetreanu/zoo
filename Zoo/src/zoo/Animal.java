/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zoo;

import java.util.ArrayList;

/**
 *
 * @author adina
 */
// 

// clasa abstracta de baza Animal 
// o clasa abstracta este o clasa care contine metode implmentate si neimplementate 
// e poate fii doar extinsa nu instantiata

public abstract class Animal {
    
    //incapsulare
    private String name;
    private String sound;
    //compozitie
    //arraylist
    private ArrayList<Tarc> tarcuri;
    
    //constructor
    public Animal(String name, String sound){
        this.name=name;
        this.sound=sound;
        this.tarcuri = new ArrayList<>();
    }
    
    public void printLegNumber(){
        System.out.println("Legs: 4");
    }
    
    public void atribuieTarc(String nume){
        tarcuri.add(new Tarc(nume));
    }
    
    //metoda abstracta care trebuie implementata in subclase
    public abstract void setSound(String sound);

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSound() {
        return sound;
    }
    
    public String getSound(String sound) {
       return this.sound;
    }
    
    public String toString(){
        return "Nume clasa: " + this.getClass().getName() + "\n" + "nume animal: " + this.name + "\n" + "sunet: " + this.sound;
    }
}
