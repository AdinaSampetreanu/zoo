/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zoo;

/**
 *
 * @author adina
 */
public class AnimalFactory {
    
    
    public static Animal getAnimal(String tipAnimal, String nume, String sunet){
        
        switch(tipAnimal){
            case "cal": return new Cal(nume, sunet);
            case "caine": return new Caine(nume, sunet);
            case "pisica": return new Pisica(nume, sunet);
            default: throw new IllegalArgumentException("Tipul animalului este invalid");
        }
        
    }
   
}
