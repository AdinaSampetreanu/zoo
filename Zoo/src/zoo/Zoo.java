/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zoo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author adina
 */
public class Zoo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Thread
        (new Thread(new ThreadReclama())).start();
        
        //Creaza lista de animale = animaleZoo
        List<Animal> animaleZoo = new ArrayList<>();
        
        //Factory method
        Animal pisica = AnimalFactory.getAnimal("pisica", "Kitty", "Miau");
        pisica.atribuieTarc("Tarcul numarul 3");
        Animal caine = AnimalFactory.getAnimal("caine", "Zeus", "HamHam");
        caine.atribuieTarc("Tarcul numarul 1- zi");
        caine.atribuieTarc("Tarcul numarul 1- noapte");
        Animal cal = AnimalFactory.getAnimal("cal", "John", "Nhhhhh");
        cal.atribuieTarc("Tarcul numarul 2- zi");
        
       animaleZoo.add(pisica);
       animaleZoo.add(caine);
       animaleZoo.add(cal);
       
       for(int i = 0 ;i < animaleZoo.size(); i++){
           System.out.println(animaleZoo.get(i).toString());
       }
       
       
       //in caz de eroare
       try {
           Thread.sleep(5000);
       }
       catch (InterruptedException e ){
           e.printStackTrace();
       }
        System.out.println("Asteptam sa vizitam animalul favorit");
    }
    
}
